#pragma once

const int BLOCK_SIZE = 10 * 830710;
const int N = 100000000;
const int TOTAL_BLOCKS = N / BLOCK_SIZE + (N % BLOCK_SIZE ? 1 : 0);