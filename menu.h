#pragma once

#include <iostream>
#include <conio.h>
#include "multithreading_winapi.h"
#include "multithreading_openOMP.h"

using namespace std;


int menu() {
	system("cls");
	cout << "---[MENU]---\n";
	cout << "for winapi count enter 0\n";
	cout << "for openOMP count enter 1\n";
	cout << "to terminate the application enter 2\n";
	cout << "> ";

	int choise;
	cin >> choise;
	switch (choise) {
	case 0:
		winapi_multithreading();
		break;
	case 1:
		omp_multithreading();
		break;
	case 2:
		return 0;
		break;
	default:
		break;
	}
	_getch();
	system("cls");
	return 1;
 }