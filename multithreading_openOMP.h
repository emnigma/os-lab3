#pragma once

#include <windows.h>
#include <iostream>
#include <omp.h>
#include <iomanip>
#include "constants.h"


using namespace std;


double omp_multithreading() {
	long double pi = 0;
	int threadCount;
	cout << "Enter number of threads: \n>";
	cin >> threadCount;
	DWORD timeStart = GetTickCount64();
	long double x = 0;
	omp_set_dynamic(0);
#pragma omp parallel for schedule(dynamic, BLOCK_SIZE) private(x) reduction(+:pi)num_threads(threadCount)
	for (long i = 0; i < N; i++)
	{
		x = (i + 0.5) / N;
		pi += 4 / (1 + x * x);
	}
	pi = pi / N;
	DWORD timeEnd = GetTickCount64();
	cout << "pi = " << setprecision(50) << pi << endl;
	cout << "Time: " << timeEnd - timeStart << " ms" << endl;
	return 0;
}
