#pragma once
#include <windows.h>
#include <iostream>
#include <iomanip>
#include "constants.h"


using namespace std;

volatile LONG nextBlock = 0;
DWORD WINAPI calculate_next(LPVOID p);
HANDLE* threadHandle;
double resultingPi = 0;

int winapi_multithreading()
{
    int threadCount;
    cout << "Enter number of threads: \n>";
    cin >> threadCount;
    threadHandle = new HANDLE[threadCount];
    for (int i = 0; i < threadCount; i++) {
        //creating thread as suspended
        threadHandle[i] = CreateThread(NULL, 0, calculate_next, (LPVOID)i, CREATE_SUSPENDED, NULL);
        if (!threadHandle[i]) {
            cout << "Error while creating thread" << endl;
            exit(1);
        }
    }

    //starting from thread count block value
    nextBlock = threadCount;
    DWORD timeStart = GetTickCount64();
    for (int i = 0; i < threadCount; i++) {
        ResumeThread(threadHandle[i]);
    }

    WaitForMultipleObjects(threadCount, threadHandle, true, INFINITE);

    resultingPi = resultingPi / N;
    DWORD timeEnd = GetTickCount64();
    cout << "pi = " << setprecision(50) << resultingPi << endl;
    cout << "Time: " << timeEnd - timeStart << " ms" << endl;
    //closing handles
    for (int i = 0; i < threadCount; i++)
        CloseHandle(threadHandle[i]);
    
    //cleaning memory
    delete[] threadHandle;
    return 0;
}

DWORD WINAPI calculate_next(LPVOID p)
{
    long double partOfPi = 0;
    int blockIndex = (long)p;
    while (blockIndex < TOTAL_BLOCKS) {
        long runI = BLOCK_SIZE * blockIndex;
        long endI = BLOCK_SIZE * (blockIndex + 1);
        for (long i = runI; (i < endI) && (i < N); i++) {
            long double x = (i + 0.5) / N;
            partOfPi = partOfPi + 4 / (1 + x * x);
        }
        blockIndex = InterlockedExchangeAdd(&nextBlock, 1);
    }
    resultingPi = resultingPi + partOfPi;
    return 0;
}